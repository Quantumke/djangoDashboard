from django.db import models
import datetime
# Create your models here.

class LoansRepaid(models.Model):
	mobile_number=models.CharField(max_length=100)
	amount=models.IntegerField()
	date=models.DateTimeField(default=datetime.datetime.now)
	class Meta:
		ordering=['-date']
		verbose_name='LoansRepaid'
		verbose_name_plural='Loans Repaid'
	def __str__(self):
		return self.mobile_number
