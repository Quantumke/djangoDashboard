from django.contrib import admin
from django.contrib.admin.views.main import ChangeList
from django.db.models import Sum, Count

from metrics.models import LoansRepaid
# Register your models here.

class LoansRepaidChangeList(ChangeList):
	def get_results(self, request):
		super(LoansRepaidChangeList,self).get_results(request)
		import datetime
		from django.db.models import Sum
		from metrics.models import LoansRepaid
		from datetime import timedelta
		todaydata = \
		LoansRepaid.objects.filter(date__gte=datetime.datetime.now() - timedelta(days=1)).aggregate(Sum('amount'))[
			'amount__sum']

		d = datetime.timedelta(days=2)  # yesterdat
		datex = datetime.datetime.today() - d  # dayb4 yesterday

		d2 = datetime.timedelta(days=1)
		datey = datetime.datetime.today() - d2
		yesterdaydata = LoansRepaid.objects.filter(date__gte=datex).filter(date__lte=datey).aggregate(Sum('amount'))[
			'amount__sum']

		d3 = datetime.timedelta(days=3)  # yesterdat
		datez = datetime.datetime.today() - d3  # dayb4 yesterday
		d4 = datetime.timedelta(days=2)
		datea = datetime.datetime.today() - d4
		iodata = LoansRepaid.objects.filter(date__gte=datez).filter(date__lte=datea).aggregate(Sum('amount'))[
			'amount__sum']

		# create dict
		response = {}
		response['data'] = {}
		response['data']['labels'] = ['day1', 'day2', 'day3']
		response['data']['datasets'] = [{
			'label': 'Loans Repaid',
			'data': [todaydata, yesterdaydata, iodata],
			'backgroundColor': [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            'borderColor': [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            'borderWidth': 1
		}]
		self.data=response['data']
@admin.register(LoansRepaid)
class LoansRepaidAdmin(admin.ModelAdmin):
	change_list_template = 'admin/loans.html'
	def get_changelist(self, request, **kwargs):
		return LoansRepaidChangeList
	def changelist_view(self, request, extra_context=None):
		try:
			response=super().changelist_view(request,extra_context=extra_context)
			queryset=response.context_data['cl'].queryset
			totalvalues={
				'totalrepayments':Count('id'),
				'totalrepaymentamounts':Sum('amount')
			}
			response.context_data['data']=list(
				queryset
				.values('id','mobile_number','amount')
				.order_by('id')
				.annotate(**totalvalues)
			)
			response.context_data['totals']=dict(
				queryset.aggregate(**totalvalues)
			)
			return response
		except Exception as e:
			return response